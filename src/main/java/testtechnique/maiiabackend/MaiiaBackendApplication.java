package testtechnique.maiiabackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaiiaBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(MaiiaBackendApplication.class, args);
    }

}
