package testtechnique.maiiabackend.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import testtechnique.maiiabackend.model.Post;

public interface PostRepository extends MongoRepository<Post, String> {
}
