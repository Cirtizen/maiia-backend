package testtechnique.maiiabackend.controller;

import org.springframework.web.bind.annotation.*;
import testtechnique.maiiabackend.service.IPostService;
import testtechnique.maiiabackend.service.dto.PostDto;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/post")
public class PostController {

    @Resource
    private IPostService postService;

    @CrossOrigin
    @GetMapping
    public List<PostDto> getPosts() {
        return postService.getPosts();
    }

    @CrossOrigin
    @PutMapping
    public void retrievingPostsFromTypicode() throws IOException, InterruptedException {
        this.postService.insertPostFromTypicode();
    }

}
