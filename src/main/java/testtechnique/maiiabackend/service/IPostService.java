package testtechnique.maiiabackend.service;

import testtechnique.maiiabackend.service.dto.PostDto;

import java.io.IOException;
import java.util.List;

public interface IPostService {

    /**
     * Get post from "https://jsonplaceholder.typicode.com/posts" and insert them indo database
     */
    void insertPostFromTypicode() throws IOException, InterruptedException;

    /**
     * Get posts from database ordered by title
     */
    List<PostDto> getPosts();
}
