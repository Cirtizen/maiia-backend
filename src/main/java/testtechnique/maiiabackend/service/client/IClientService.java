package testtechnique.maiiabackend.service.client;

import testtechnique.maiiabackend.model.Post;

import java.io.IOException;
import java.util.List;

public interface IClientService {
    List<Post> getPostFromTypicode() throws IOException, InterruptedException;
}
