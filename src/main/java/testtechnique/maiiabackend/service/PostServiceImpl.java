package testtechnique.maiiabackend.service;

import org.springframework.stereotype.Service;
import testtechnique.maiiabackend.model.Post;
import testtechnique.maiiabackend.repository.PostRepository;
import testtechnique.maiiabackend.service.builder.PostDtoBuilder;
import testtechnique.maiiabackend.service.client.IClientService;
import testtechnique.maiiabackend.service.dto.PostDto;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements IPostService {
    private final static Integer NUMBER_OF_POSTS_RETRIEVING = 50;

    @Resource
    private PostRepository postRepository;

    @Resource
    private IClientService clientService;

    @Override
    public void insertPostFromTypicode() throws IOException, InterruptedException {
        List<Post> posts = this.clientService.getPostFromTypicode().stream()
                .limit(NUMBER_OF_POSTS_RETRIEVING)
                .collect(Collectors.toList());
        this.postRepository.saveAll(posts);
    }

    @Override
    public List<PostDto> getPosts() {
        return this.postRepository.findAll().stream()
                .map(post -> new PostDtoBuilder(post).build())
                .sorted(Comparator.comparing(PostDto::getTitle))
                .collect(Collectors.toList());
    }
}
