package testtechnique.maiiabackend.service.builder;

import testtechnique.maiiabackend.model.Post;
import testtechnique.maiiabackend.service.dto.PostDto;

public class PostDtoBuilder {
    private Post post;

    public PostDtoBuilder(Post post) {
        this.post = post;
    }

    public PostDto build() {
        return new PostDto(post.getTitle(), post.getBody());
    }
}
