package testtechnique.maiiabackend.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import testtechnique.maiiabackend.service.dto.PostDto;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class IPostServiceIntegrationTest {

    @Resource
    private IPostService postService;

    @BeforeEach
    void setUp() throws IOException, InterruptedException {
        this.postService.insertPostFromTypicode();
    }

    @Test
    void getPosts() {
        List<PostDto> posts = this.postService.getPosts();
        assertEquals(50, posts.size());
        assertEquals("a quo magni similique perferendis", posts.get(0).getTitle());
        assertEquals("alias dolor cumque\nimpedit blanditiis non eveniet odio maxime\nblanditiis amet eius quis tempora quia autem rem\na provident perspiciatis quia", posts.get(0).getBody());
    }
}