# Maiia-backend

Pour démarrer le projet :
    - Java 11 minimum
    - Mettre à jour la configuration mongodb si besoin (configuration par defaut actuellement)
    - Avoir Gradle d'installé puis executer la commande gradle build.
    - Lancer l'application via le main ou avec la commande gradlew bootRun
    
Reste à faire / corriger : 
    - Les tests n'utilisent pas la base H2 (ou utiliser de.flapdoodle.embed:de.flapdoodle.embed.mongo)
    - Mocker IClientService dans les tests.